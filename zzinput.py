import numpy as np

xlabels = [r'Combined', r'$4e$', r'$2e2\mu$', r'$4\mu$']

atlas_label = 'ATLAS'
atlas_nominal = np.array([46.2, 13.7, 20.9, 11.5])
atlas_stat_hi = np.array([1.5, 0.9, 1.0, 0.7])
atlas_stat_lo = atlas_stat_hi
atlas_syst_hi = np.array([1.2, 0.4, 0.6, 0.4])
atlas_syst_lo = np.array([1.1, 0.4, 0.6, 0.4])
atlas_lumi_hi = np.array([1.6, 0.5, 0.7, 0.4])
atlas_lumi_lo = np.array([1.4, 0.4, 0.6, 0.4])
atlas_total_hi = np.sqrt(atlas_stat_hi**2 + atlas_syst_hi**2 + atlas_lumi_hi**2)
atlas_total_lo = np.sqrt(atlas_stat_lo**2 + atlas_syst_lo**2 + atlas_lumi_lo**2)

th_atlas_label = 'ATLAS fiducial phase space' # r'NNLO $\otimes$ NNNLO ($gg$) $\otimes$ NLO EW'
th_atlas_nominal = np.array([42.8570, 10.8518, 21.1535, 10.8518])
th_atlas_total_hi = np.array([1.8720, 0.4728, 0.9263, 0.4728])
th_atlas_total_lo = np.array([1.5260, 0.3855, 0.7550, 0.3855])

atlas_sig_upwards = np.maximum((atlas_nominal - th_atlas_nominal) / np.sqrt(atlas_total_lo**2 + th_atlas_total_hi**2), 0.)
atlas_sig_downwards = np.minimum((atlas_nominal - th_atlas_nominal) / np.sqrt(atlas_total_hi**2 + th_atlas_total_lo**2), 0.)
atlas_sig = atlas_sig_upwards + atlas_sig_downwards



cms_label = 'CMS'
cms_nominal = np.array([42.2])
cms_stat_hi = np.array([1.4])
cms_stat_lo = np.array([1.4])
cms_syst_hi = np.array([1.6])
cms_syst_lo = np.array([1.5])
cms_lumi_hi = np.array([1.1])
cms_lumi_lo = np.array([1.1])
cms_total_hi = np.sqrt(cms_stat_hi**2 + cms_syst_hi**2 + cms_lumi_hi**2)
cms_total_lo = np.sqrt(cms_stat_lo**2 + cms_syst_lo**2 + cms_lumi_lo**2)

# th_cms_label = r'NLO $\oplus$ NNLO ($gg$)'
# th_cms_nominal = np.array([34.4])
# th_cms_total_hi = np.sqrt(np.array([0.7])**2 + np.array([0.5])**2)
# th_cms_total_lo = np.sqrt(np.array([0.6])**2 + np.array([0.5])**2)
th_cms_label = 'CMS fiducial phase space' # r'NNLO $\otimes$ NNNLO ($gg$) $\otimes$ NLO EW'
th_cms_nominal = np.array([38.933])
th_cms_total_hi = np.array([1.709])
th_cms_total_lo = np.array([1.390])

cms_sig_upwards = np.maximum((cms_nominal - th_cms_nominal) / np.sqrt(cms_total_lo**2 + th_cms_total_hi**2), 0.)
cms_sig_downwards = np.minimum((cms_nominal - th_cms_nominal) / np.sqrt(cms_total_hi**2 + th_cms_total_lo**2), 0.)
cms_sig = cms_sig_upwards + cms_sig_downwards



comb_label = r'ATLAS$\,$+$\,$CMS combination'
atlasfactor_sf = 0.8563636363636364
atlasfactor_df = 0.9038272816486753
atlas_ext_nominal = (atlas_nominal[1] + atlas_nominal[3]) * atlasfactor_sf + atlas_nominal[2] * atlasfactor_df
atlas_ext_stat_hi = np.sqrt((atlas_stat_hi[1]*atlasfactor_sf)**2 + (atlas_stat_hi[3]*atlasfactor_sf)**2 + (atlas_stat_hi[2]*atlasfactor_df)**2)
atlas_ext_stat_lo = np.sqrt((atlas_stat_lo[1]*atlasfactor_sf)**2 + (atlas_stat_lo[3]*atlasfactor_sf)**2 + (atlas_stat_lo[2]*atlasfactor_df)**2)
atlas_ext_syst_hi = (atlas_syst_hi[1] + atlas_syst_hi[3]) * atlasfactor_sf + atlas_syst_hi[2] * atlasfactor_df
atlas_ext_syst_lo = (atlas_syst_lo[1] + atlas_syst_lo[3]) * atlasfactor_sf + atlas_syst_lo[2] * atlasfactor_df
atlas_ext_lumi_hi = (atlas_lumi_hi[1] + atlas_lumi_hi[3]) * atlasfactor_sf + atlas_lumi_hi[2] * atlasfactor_df
atlas_ext_lumi_lo = (atlas_lumi_lo[1] + atlas_lumi_lo[3]) * atlasfactor_sf + atlas_lumi_lo[2] * atlasfactor_df
print atlas_ext_nominal, '+', np.sqrt(atlas_ext_stat_hi**2 + atlas_ext_syst_hi**2 + atlas_ext_lumi_hi**2), '-', np.sqrt(atlas_ext_stat_lo**2 + atlas_ext_syst_lo**2 + atlas_ext_lumi_lo**2)
cmsfactor = 0.9599551497677399
comb_nominal = (np.array(atlas_ext_nominal) + cms_nominal * cmsfactor) / 2. # TODO: construct combination as least-squares fit! Write separate module `least_squares_fit` taking uncorrelated and correlated uncertainties as arguments?
comb_stat_hi = np.sqrt(np.array(atlas_ext_stat_hi)**2 + (cms_stat_hi * cmsfactor)**2) / 2.
comb_stat_lo = np.sqrt(np.array(atlas_ext_stat_lo)**2 + (cms_stat_lo * cmsfactor)**2) / 2.
comb_syst_hi = np.sqrt(np.array(atlas_ext_syst_hi)**2 + (cms_syst_hi * cmsfactor)**2) / 2.
comb_syst_lo = np.sqrt(np.array(atlas_ext_syst_lo)**2 + (cms_syst_lo * cmsfactor)**2) / 2.
comb_lumi_hi = (np.array(atlas_ext_lumi_hi) + cms_lumi_hi) / 2.
comb_lumi_lo = (np.array(atlas_ext_lumi_lo) + cms_lumi_lo) / 2.
comb_total_hi = np.sqrt(comb_stat_hi**2 + comb_syst_hi**2 + comb_lumi_hi**2)
comb_total_lo = np.sqrt(comb_stat_lo**2 + comb_syst_lo**2 + comb_lumi_lo**2)
print cms_nominal * cmsfactor, '+', cms_total_hi * cmsfactor, '-', cms_total_lo * cmsfactor
print comb_nominal, '+', comb_total_hi, '-', comb_total_lo

th_comb_label = r'ATLAS$\,\cap\,$CMS fiducial phase space' # r'ATLAS$\,$+$\,$CMS prediction'
th_comb_nominal = np.array([37.447])
th_comb_total_hi = np.array([1.649])
th_comb_total_lo = np.array([1.340])
# th_comb_nominal = (np.array(th_atlas_nominal[0]) + th_cms_nominal) / 2.
# th_comb_total_hi = (np.array(th_atlas_total_hi[0]) + th_cms_total_hi) / 2.
# th_comb_total_lo = (np.array(th_atlas_total_lo[0]) + th_cms_total_lo) / 2.


comb_sig_upwards = np.maximum((comb_nominal - th_comb_nominal) / np.sqrt(comb_total_lo**2 + th_comb_total_hi**2), 0.)
comb_sig_downwards = np.minimum((comb_nominal - th_comb_nominal) / np.sqrt(comb_total_hi**2 + th_comb_total_lo**2), 0.)
comb_sig = comb_sig_upwards + comb_sig_downwards

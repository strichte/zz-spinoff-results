# Summaries and spinoff results of ZZ → 4l LHC measurements

## Data

* Measurements from ATLAS and CMS at 13 TeV, using around 36 fb<sup>-1</sup> of data (2015+2016)
  * [ATLAS paper](https://arxiv.org/abs/1709.07703)
  * [CMS paper](https://arxiv.org/abs/1709.08601)


## Theoretical predictions

*Unless mentioned otherwise in the labels:*

* Theoretical predictions are based on MATRIX NNLO (α<sup>4</sup>α<sub>s</sub><sup>2</sup>)
  * [Calculation](https://arxiv.org/abs/1507.06257)
  * [MATRIX program](https://arxiv.org/abs/1711.06631)
  * The MATRIX calculations are run by Stefan Richter using custom phase space cuts implemented by him. Email him @cern.ch for more information.
  * Many thanks to MATRIX authors Marius Wiesemann and Stefan Kallweit for their help with running the calculations
* NNNLO (α<sup>4</sup>α<sub>s</sub><sup>3</sup>) corrections are applied to loop-induced gg-initiated production
  * [Calculation](https://arxiv.org/abs/1509.06734)
* NLO (α<sup>5</sup>) electroweak corrections (weak only, excluding photonic corrections) are applied to all subprocesses *except* loop-induced gg-initiated production
  * [Calculation 1](https://arxiv.org/abs/1601.07787)
  * [Calculation 2](https://arxiv.org/abs/1611.05338)
  * Many thanks to Benedikt Biedermann (and presumably others) for providing the custom calculations, and thanks to Jochen Meyer from ATLAS for advising us and serving as contact to the theorists
* The details of all calculations (scales, PDFs, …) can be found in the [ATLAS publication](https://arxiv.org/abs/1709.07703)

## About & usage

Plots made by Stefan Richter for fun and PhD thesis. Feel free to reuse and modify them, but please give credit to the original author and cite the above publications.

The plots are **not** official ATLAS or CMS plots. They are secondary work based on published data.

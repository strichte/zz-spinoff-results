#!/usr/bin/env python

# A super inelegant but effective script for making interesting plots
# If the functionality is extended, a thorough refactoring is probably a good idea, to remove lots of code duplication
# Dependencies: numpy, matplotlib, and the zzinput module provided in the same repository

import zzinput
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.ticker import AutoMinorLocator

mpl.rcParams['lines.linewidth'] = 1.0

def plot_atlas_cms_th():
    title = r"$\mathrm{pp}\, \to\,\mathrm{ZZ}\, \to\, \ell^+\ell^-\ell'^+\ell'^-$,  13 TeV,  ~36 fb$^{-1}$ per exp." # ATLAS and CMS phase spaces differ
    
    fig, (ax0, ax1, ax2) = plt.subplots(3, sharex=True, gridspec_kw = {'height_ratios':[3, 1, 1]}, figsize=(6.4,6.4))
    
    ax1.plot([2, 15], [1., 1.], 'k-', linewidth=1.)
    ax2.plot([2, 15], [0., 0.], 'k-', linewidth=1.)
    for sigma in [1, 2, 3]:
        ax2.plot([2, 15], [sigma, sigma], '--', color='darkgrey', linewidth=1., zorder=0.3)
    ax0.set_xlim((2, 15))
    
    # ATLAS
    ax0.errorbar([3, 6, 9, 12], zzinput.atlas_nominal, yerr=[zzinput.atlas_total_lo, zzinput.atlas_total_hi], fmt='o', color='k', label=zzinput.atlas_label, elinewidth=1.5, capsize=3, markersize=5)
    ax0.errorbar([3, 6, 9, 12], zzinput.atlas_nominal, yerr=[zzinput.atlas_stat_lo, zzinput.atlas_stat_hi], fmt='o', color='k', capsize=2, markersize=0)
    ax1.errorbar([3, 6, 9, 12], zzinput.atlas_nominal/zzinput.th_atlas_nominal, yerr=[zzinput.atlas_total_lo/zzinput.th_atlas_nominal, zzinput.atlas_total_hi/zzinput.th_atlas_nominal], fmt='o', color='k', elinewidth=1.5, capsize=3, markersize=5)
    
    # ATLAS prediction
    for centre, nominal, lo, hi, sig in zip([3, 6, 9, 12], zzinput.th_atlas_nominal, zzinput.th_atlas_total_lo, zzinput.th_atlas_total_hi, zzinput.atlas_sig):
        if centre < 4:
            ax0.fill_between([centre-0.48, centre+0.48], [nominal-lo, nominal-lo], [nominal+hi, nominal+hi], label=zzinput.th_atlas_label, color='gold', zorder=0.5)
        else:
            ax0.fill_between([centre-0.48, centre+0.48], [nominal-lo, nominal-lo], [nominal+hi, nominal+hi], color='gold', zorder=0.5)
        ax0.plot([centre-0.48, centre+0.48], [nominal, nominal], color='darkgoldenrod', zorder=0.6)
        # Ratio:
        if centre < 4:
            ax1.fill_between([centre-0.5, centre+0.5], np.array([nominal-lo, nominal-lo])/nominal, np.array([nominal+hi, nominal+hi])/nominal, color='darkorange', zorder=0.5, label=r'$\pm 1\sigma$')
            ax1.fill_between([centre-0.5, centre+0.5], np.array([nominal-2*lo, nominal-2*lo])/nominal, np.array([nominal+2*hi, nominal+2*hi])/nominal, color='gold', zorder=0.4, label=r'$\pm 2\sigma$')
            ax2.fill_between([centre-0.15, centre+0.15], [sig, sig], [0, 0], color='k', zorder=0.5)
        else:
            ax1.fill_between([centre-0.5, centre+0.5], np.array([nominal-lo, nominal-lo])/nominal, np.array([nominal+hi, nominal+hi])/nominal, color='darkorange', zorder=0.5)
            ax1.fill_between([centre-0.5, centre+0.5], np.array([nominal-2*lo, nominal-2*lo])/nominal, np.array([nominal+2*hi, nominal+2*hi])/nominal, color='gold', zorder=0.4)
            ax2.fill_between([centre-0.15, centre+0.15], [sig, sig], [0, 0], color='k', zorder=0.5)     
    
    # CMS
    ax0.errorbar([4], zzinput.cms_nominal, yerr=[zzinput.cms_total_lo, zzinput.cms_total_hi], fmt='s', color='mediumblue', label=zzinput.cms_label, elinewidth=1.5, capsize=3, markersize=5)
    ax0.errorbar([4], zzinput.cms_nominal, yerr=[zzinput.cms_stat_lo, zzinput.cms_stat_hi], fmt='s', color='mediumblue', capsize=2, markersize=0)
    ax1.errorbar([4], zzinput.cms_nominal/zzinput.th_cms_nominal, yerr=[zzinput.cms_total_lo/zzinput.th_cms_nominal, zzinput.cms_total_hi/zzinput.th_cms_nominal], fmt='s', color='mediumblue', elinewidth=1.5, capsize=3, markersize=5)
    
    # CMS prediction
    for centre, nominal, lo, hi, sig in zip([4], zzinput.th_cms_nominal, zzinput.th_cms_total_lo, zzinput.th_cms_total_hi, zzinput.cms_sig):
        if centre < 5:
            ax0.fill_between([centre-0.48, centre+0.48], [nominal-lo, nominal-lo], [nominal+hi, nominal+hi], label=zzinput.th_cms_label, color='deepskyblue', zorder=0.5)
        else:
            ax0.fill_between([centre-0.48, centre+0.48], [nominal-lo, nominal-lo], [nominal+hi, nominal+hi], color='deepskyblue', zorder=0.5)
        ax0.plot([centre-0.48, centre+0.48], [nominal, nominal], color='royalblue', zorder=0.6)
        # Ratio:
        ax1.fill_between([centre-0.5, centre+0.5], np.array([nominal-lo, nominal-lo])/nominal, np.array([nominal+hi, nominal+hi])/nominal, color='darkorange', zorder=0.5)
        ax1.fill_between([centre-0.5, centre+0.5], np.array([nominal-2*lo, nominal-2*lo])/nominal, np.array([nominal+2*hi, nominal+2*hi])/nominal, color='gold', zorder=0.4)
        ax2.fill_between([centre-0.15, centre+0.15], [sig, sig], [0, 0], color='mediumblue', zorder=0.5)
    
    # ATLAS + CMS combination
    ax0.errorbar([5], zzinput.comb_nominal, yerr=[zzinput.comb_total_lo, zzinput.comb_total_hi], fmt='D', color='darkred', label=zzinput.comb_label, elinewidth=1.5, capsize=3, markersize=5)
    ax0.errorbar([5], zzinput.comb_nominal, yerr=[zzinput.comb_stat_lo, zzinput.comb_stat_hi], fmt='D', color='darkred', capsize=2, markersize=0)
    ax1.errorbar([5], zzinput.comb_nominal/zzinput.th_comb_nominal, yerr=[zzinput.comb_total_lo/zzinput.th_comb_nominal, zzinput.comb_total_hi/zzinput.th_comb_nominal], fmt='D', color='darkred', elinewidth=1.5, capsize=3, markersize=5)

    # ATLAS + CMS combination prediction
    for centre, nominal, lo, hi, sig in zip([5], zzinput.th_comb_nominal, zzinput.th_comb_total_lo, zzinput.th_comb_total_hi, zzinput.comb_sig):
        if centre < 6:
            ax0.fill_between([centre-0.48, centre+0.48], [nominal-lo, nominal-lo], [nominal+hi, nominal+hi], label=zzinput.th_comb_label, color='salmon', zorder=0.5)
        else:
            ax0.fill_between([centre-0.48, centre+0.48], [nominal-lo, nominal-lo], [nominal+hi, nominal+hi], color='salmon', zorder=0.5)
        ax0.plot([centre-0.48, centre+0.48], [nominal, nominal], color='firebrick', zorder=0.6)
        # Ratio:
        ax1.fill_between([centre-0.5, centre+0.5], np.array([nominal-lo, nominal-lo])/nominal, np.array([nominal+hi, nominal+hi])/nominal, color='darkorange', zorder=0.5)
        ax1.fill_between([centre-0.5, centre+0.5], np.array([nominal-2*lo, nominal-2*lo])/nominal, np.array([nominal+2*hi, nominal+2*hi])/nominal, color='gold', zorder=0.4)
        ax2.fill_between([centre-0.15, centre+0.15], [sig, sig], [0, 0], color='darkred', zorder=0.5)

    ax0.legend(title=r'NNLO $\otimes$ NNNLO (gg) $\otimes$ NLO-weak prediction', frameon=True)
    ax0.set_title(title, loc='left', size='x-large')

    ax1.legend(frameon=True)
    
    ax0.set_ylabel('Cross section (fb)', labelpad=20, fontsize='large')
    #ax1.set_ylabel('Meas. / pred.', labelpad=20, fontsize='large')
    ax1.set_ylabel(r'$\frac{\mathrm{Measurement}}{\mathrm{Prediction}}$', labelpad=17, fontsize='x-large')
    ax2.set_ylabel('Discrepancy', labelpad=14, fontsize='large')

    ax2.set_ylim((-1., 4.))
    
    # ATLAS style
    ax0.tick_params(which='both', bottom='off', top='off', left='on', right='on', direction='in')
    ax0.xaxis.set_minor_locator(AutoMinorLocator(5))
    ax1.tick_params(which='both', bottom='off', top='off', left='on', right='on', direction='in')
    ax1.tick_params(axis='x', pad=5)
    ax1.xaxis.set_minor_locator(AutoMinorLocator(5))
    ax2.tick_params(which='both', bottom='off', top='off', left='on', right='on', direction='in')
    ax2.tick_params(axis='x', pad=5)
    ax2.xaxis.set_minor_locator(AutoMinorLocator(5))
    
    fig.canvas.draw()
    
    sigma_ticks = [-1, 0, 1, 2, 3, 4]
    ax2.set_yticks(sigma_ticks)
    ax2.set_yticklabels([str(s).replace('-', '$-$') + r'$\sigma$' for s in sigma_ticks])
    
    ax2.set_xticks([4, 7, 10, 13])
    ax2.set_xticklabels(zzinput.xlabels, fontsize='large')
    
    fig.subplots_adjust(left=0.15, right=0.95, top=0.95, bottom=0.1, hspace=0.07)    
    fig.savefig('results/fiducial_atlas_cms.pdf')



def main():
    plot_atlas_cms_th()



if __name__ == '__main__':
    main()
